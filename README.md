# Pixew Front

Este é o repositório para o front-end do projeto Pixew dos integrantes Pedro Tonini Rosenberg Schneider e Clara Yuki Sano.

## Sobre o projeto

Este projeto consiste em uma plataforma para usuários darem notas e registrarem suas opiniões em formato de reviews para jogos. 

## Tecnologias

O front-end da aplicação será feito utilizando a framework de front [Svelte](https://svelte.dev/).