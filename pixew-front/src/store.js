import { writable, derived } from "svelte/store";

export const loggedUserStore = writable({});
export const trendingReviewsStore = writable([]);
export const newReviewsStore = writable([]);
export const trendingGamesStore = writable([]);
export const allGamesStore = writable([]);
export const searchGamesStore = writable([]);